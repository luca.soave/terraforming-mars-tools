import Vue from 'vue';
import store from '@/store';
import Home from '@/home/Home';

describe('Home.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Home);
    const vm = new Constructor({
      store
    }).$mount();
    expect(vm.$el.querySelector('.home h1').textContent)
      .to.equal('Welcome to Terraforming Mars Tools');
  });
});
