// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'core-js/es6/promise';
import Vue from 'vue';
import App from './App';
import router from './core/app-routes';
import store from './store';
import firebase from 'firebase/app';
import 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyB-7XV8NCfPiQUgMIuTXw6XlN2JdFCEU9A',
  authDomain: 'terraforming-mars-tools.firebaseapp.com',
  databaseURL: 'https://terraforming-mars-tools.firebaseio.com',
  storageBucket: 'terraforming-mars-tools.appspot.com',
  messagingSenderId: '524554196825'
};

firebase.initializeApp(firebaseConfig);

export const database = firebase.database();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});

if (process.env.NODE_ENV === 'production') {
  (function () {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/service-worker.js');
    }
  })();
}
