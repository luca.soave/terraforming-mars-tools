import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/home/Home';

const Game = () => import('@/game/Game');

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/game/:gameId',
      component: Game
    }
  ]
});
