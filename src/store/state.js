import { HashGenerator } from '@/shared/HashGenerator';
export const PLAYER_ID_STORAGE_KEY = 'terraforming_mars_tools_player_id';
export const GAME_ID_STORAGE_KEY = 'terraforming_mars_tools_game_id';
export const PLAYER_NAME_STORAGE_KEY = 'terraforming_mars_tools_player_name';

let playerId = JSON.parse(window.localStorage.getItem(PLAYER_ID_STORAGE_KEY));
if (!playerId) {
  playerId = HashGenerator.generateHash();
  window.localStorage.setItem(PLAYER_ID_STORAGE_KEY, playerId);
}

let gameId = JSON.parse(window.localStorage.getItem(GAME_ID_STORAGE_KEY));
if (!gameId) {
  gameId = HashGenerator.generateHash();
  window.localStorage.setItem(GAME_ID_STORAGE_KEY, gameId);
}

const playerName = window.localStorage.getItem(PLAYER_NAME_STORAGE_KEY) || null;

export const state = {
  playerId,
  gameId,
  playerName,
  game: null
};
