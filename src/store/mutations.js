import Vue from 'vue';
import { GAME_ID_STORAGE_KEY, PLAYER_NAME_STORAGE_KEY } from './state';

export const changeGameState = (state, { newState }) => {
  Vue.set(state, 'game', newState);
};

export const setGameId = (state, { gameId }) => {
  state.gameId = gameId;
  window.localStorage.setItem(GAME_ID_STORAGE_KEY, state.gameId);
};

export const setPlayerName = (state, { playerName }) => {
  state.playerName = playerName;
  window.localStorage.setItem(PLAYER_NAME_STORAGE_KEY, playerName);
};
