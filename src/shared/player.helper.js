export const startingResources = {
  tr: 20,
  me: 0,
  meP: 1,
  steel: 0,
  steelP: 1,
  titanium: 0,
  titaniumP: 1,
  plants: 0,
  plantsP: 1,
  energy: 0,
  energyP: 1,
  heat: 0,
  heatP: 1
};

export const resourceKeys = [
  'me',
  'steel',
  'titanium',
  'plants',
  'energy',
  'heat'
];
